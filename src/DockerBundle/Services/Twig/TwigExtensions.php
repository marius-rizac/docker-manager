<?php

namespace DockerBundle\Services\Twig;

class TwigExtensions extends \Twig_Extension
{
    /**
     * @return array
     */
    public function getFunctions()
    {
        return [
            DrawEsStatusFunction::create(),
            DrawSimpleTableFunction::create(),
            DrawTableImageFunction::create(),
            DrawTableNetworksFunction::create(),
            DrawTableProcessFunction::create(),
            DrawTableStatsFunction::create(),
            DrawVolumesFunction::create(),
            MatchFunction::create(),
        ];
    }
}
