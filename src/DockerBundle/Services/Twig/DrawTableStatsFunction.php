<?php

namespace DockerBundle\Services\Twig;

use DockerBundle\Services\ResponseTable;
use Kisphp\Twig\AbstractTwigFunction;
use Kisphp\Twig\IsSafeHtml;

class DrawTableStatsFunction extends AbstractTwigFunction
{
    use IsSafeHtml;

    /**
     * @return string
     */
    protected function getExtensionName()
    {
        return 'drawTableStats';
    }

    /**
     * @return \Closure
     */
    protected function getExtensionCallback()
    {
        return function (array $tableData) {
            return $this->drawTable($tableData);
        };
    }

    /**
     * @param array $tableData
     *
     * @return string
     */
    protected function drawTable(array $tableData)
    {
        return ResponseTable::drawStats($tableData);
    }
}
