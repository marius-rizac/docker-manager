<?php

namespace DockerBundle\Services;

use Symfony\Component\HttpFoundation\Response;

class Docker
{
    const DELIMITER = ':::';

    /**
     * @var Runner
     */
    private $run;

    /**
     * @var \DockerBundle\Services\ResponseParser
     */
    private $parser;

    /**
     * @param \DockerBundle\Services\Runner $runner
     * @param \DockerBundle\Services\ResponseParser $parser
     */
    public function __construct(Runner $runner, ResponseParser $parser)
    {
        $this->run = $runner;
        $this->parser = $parser;
    }

    /**
     * @param string $id
     *
     * @return string
     */
    public function removeProcess($id)
    {
        return $this->run->removeProcess($id);
    }

    /**
     * @param string $id
     *
     * @return int
     */
    public function removeVolume($id)
    {
        return $this->run->removeVolume($id);
    }

    /**
     * @param string $id
     *
     * @return int
     */
    public function removeNetwork($id)
    {
        return $this->run->removeDockerNetwork($id);
    }

    /**
     * @param string $id
     *
     * @return string
     */
    public function removeDockerImage($id)
    {
        return $this->run->removeDockerImage($id);
    }

    /**
     * @return array
     */
    public function getImages()
    {
        $images = $this->run->getImages();

        return $this->parser->parseImagesResponse($images);
    }

    /**
     * @return array
     */
    public function getProcesses()
    {
        $processes = $this->run->getProcesses();

        return $this->parser->parseProcessResponse($processes);
    }

    /**
     * @return array
     */
    public function getNetworks()
    {
        $networks = $this->run->getNetworks();

        return $this->parser->parseNetworksResponse($networks);
    }

    /**
     * @return array
     */
    public function getStats()
    {
        return $this->parser->parseStatsResponse(
            $this->run->getStats()
        );
    }

    /**
     * @return array
     */
    public function getVolumes()
    {
        return $this->parser->parseVolumesResponse(
            $this->run->getVolumes()
        );
    }

    /**
     * @return array|\Symfony\Component\HttpFoundation\Response
     */
    public function getEsClusterHealth()
    {
        $content = $this->run->getClusterHealth();

        if (\is_array($content) === false) {
            return new Response($content);
        }

        return $this->parser->parseEsClusterHealth(
            (string) $content
        );
    }

    /**
     * @return array|\Symfony\Component\HttpFoundation\Response
     */
    public function getEsIndices()
    {
        $content = $this->run->getIndices();

        if (\is_array($content) === false) {
            return new Response($content);
        }

        return $this->parser->parseEsIndices(
            (string) $content
        );
    }

    /**
     * @return array|\Symfony\Component\HttpFoundation\Response
     */
    public function getEsStatus()
    {
        $content = $this->run->getInfo();

        if (\is_array($content) === false) {
            return new Response($content);
        }

        return $this->parser->parseEsInfo(
            (string) $content
        );
    }
}
