.PHONY: i run t m

i:
	@echo "Install"
	./build.sh dev

run:
	@echo "Run"
	php -S localhost:8080 -t web

t:
	@echo "Run tests"
	vendor/bin/phpunit

m:
	@echo "Run metrics analysis"
	rm -rf _metrics
	vendor/bin/phpmetrics --report-html=_metrics .
