function writeAlert(message) {
  let html = '<div class="alert alert-danger">' + message + '</div>';

  $('#announce').fadeIn('slow').html(html);

  setTimeout(function(){
    $('#announce').fadeOut('slow').html();
  }, 5000);
}

function createRowId(imageId) {
  return imageId.replace(':', '-');
}

const Docker = new function () {
  let self = this;
  
  this.removeRow = function(rowClass){
    $('.row-' + createRowId(rowClass)).addClass('danger').fadeOut('slow');
  };

  let createRequest = function(ajaxUrl, params){
    return $.ajax({
      url: ajaxUrl,
      type: "POST",
      dataType: "json",
      data: params
    });
  };

  this.deleteImage = function(id){
    let request = createRequest('/delete/image/' + id, {});

    request.done(function(){
      return self.removeRow(id);
    });
  };
  this.deleteNetwork = function(element){
    let row = element.parent().parent();
    let id = element.data('network-id');

    let request = createRequest('/delete/network/' + id, {});

    request.done(function(){
      return self.removeRow(row);
    });
  };
  this.deleteProcess = function(id){
    let request = createRequest('/delete/process/' + id, {});

    request.done(function(){
      return self.removeRow(id);
    });
  };
  this.deleteVolume = function(element){
    let row = element.parent().parent();
    let id = element.data('volume-id');

    let request = createRequest('/delete/volume/' + id, {});

    request.done(function(r){
      if (r.code === 0) {
        return self.removeRow(row);
      } else {
        writeAlert('Could not remove volume');
      }
    });
  };
  this.toggleReload = function(){

    let element = $('.toggle-reload').find('.fa');

    if (toggleReload === true) {
      toggleReload = false;
      element.removeClass('fa-toggle-on').addClass('fa-toggle-off');
      return;
    }

    toggleReload = true;
    element.addClass('fa-toggle-on').removeClass('fa-toggle-off');

    let ds = new DockerStats();
    ds.getStats();
  };
};

let DockerStats = function(){
  let self = this;

  self.interval = 5000;

  if (typeof toggleReload === 'undefined') {
    toggleReload = false;
  }

  self.getStats = function(){
    if (toggleReload === false) {
      return;
    }
    $.ajax({
      url: '/stats/get',
      type: "GET"
    }).done(function(r){
      $('#docker-stats').html(r);
      setTimeout(function(){
        self.getStats();
      }, self.interval);
    });
  };
};

module.exports = {
  init: () => {
    $('.delete-process').click(function(e){
      e.preventDefault();

      Docker.deleteProcess($(this).data('process-id'));
    });
    $('.delete-image').click(function(e){
      e.preventDefault();

      Docker.deleteImage($(this).data('image-id'));
    });
    $('.delete-network').click(function(e){
      e.preventDefault();

      Docker.deleteNetwork($(this));
    });
    $('.delete-volume').click(function(e){
      e.preventDefault();

      Docker.deleteVolume($(this));
    });

    $('.toggle-reload').click(function(e){
      e.preventDefault();

      Docker.toggleReload();
    });

    let imagesToDelete = [];

    $('.select-images').click(function(e){
      let elem = $(this);

      let imageId = elem.data('image-id');

      if (elem.is(':checked')) {
        if (imagesToDelete.indexOf(imageId) > -1) {
          e.preventDefault();
          return;
        }
        imagesToDelete.push(imageId);
      } else {
        let index = imagesToDelete.indexOf(imageId);
        if (index > -1) {
          imagesToDelete.splice(index, 1);
        }
      }
    });

    $('#delete-selected-images').click(function(e){
      e.preventDefault();

      if (imagesToDelete.length > 0) {
        imagesToDelete.forEach(function(item){
          Docker.deleteImage(item);
        });
      }

      imagesToDelete = [];
    });

    let processesToDelete = [];

    $('.select-process').click(function(e){
      let elem = $(this);

      let processId = elem.data('process-id');

      if (elem.is(':checked')) {
        if (processesToDelete.indexOf(processId) > -1) {
          e.preventDefault();
          return;
        }
        processesToDelete.push(processId);
      } else {
        let index = processesToDelete.indexOf(processId);
        if (index > -1) {
          processesToDelete.splice(index, 1);
        }
      }
    });

    $('#delete-selected-processes').click(function(e){
      e.preventDefault();

      if (processesToDelete.length > 0) {
        processesToDelete.forEach(function(item){
          Docker.deleteProcess(item);
        });
      }

      processesToDelete = [];
    });

    let ds = new DockerStats();
    setTimeout(function(){
      ds.getStats();
    }, ds.interval);
  }
};
