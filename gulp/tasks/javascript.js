'use strict';

let gulp = require('gulp');
let concat = require('gulp-concat');
let plumber = require('gulp-plumber');
let strip = require('gulp-strip-comments');
let browserify = require('gulp-browserify');
let minify = require('gulp-minify');

let config = require('../config');

gulp.task('js-external', function(){
  return gulp.src([
    'node_modules/jquery/dist/jquery.min.js',
    'node_modules/bootstrap/dist/js/bootstrap.min.js'
  ])
    .pipe(plumber())
    .pipe(strip())
    .pipe(concat('external.js'))
    .pipe(gulp.dest(config.publicDir + 'js/'))
  ;
});

gulp.task('js', function(){
  return gulp.src(config.sourceJsDir + 'app.js')
    .pipe(plumber())
    .pipe(browserify({
      insertGlobals: true,
      debug: true
    }))
    .pipe(strip())
    .pipe(minify({
      noSource: true
    }))
    .pipe(concat('app.js'))
    .pipe(gulp.dest(config.publicDir + 'js/'))
  ;
});

gulp.task('watch:js', function(){
  gulp.watch(config.sourceJsDir + '**/*.js', ['js']);
});

let GR = require('kisphp-gulp-commander');

GR.addTask('js');
GR.addTask('js-external');

GR.addWatch('watch:js');
