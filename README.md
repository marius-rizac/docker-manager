# Simple Docker Dashboard

[![pipeline status](https://gitlab.com/marius-rizac/docker-manager/badges/master/pipeline.svg)](https://gitlab.com/marius-rizac/docker-manager/commits/master)

### Requirements

- php >= 7.1
- npm >= 3.10
- gulp

### Installation

```bash
composer install -n
npm install
npm run gulp
```

Or run automatic installation if you have GNU Make available

```bash
make

# or

make i
```

### Run

```bash
make run

# or

php -S localhost:8000 -t public
```
Then open it in [http://localhost:8000](http://localhost:8000)

> Note! There is no confirmation required when it deletes an image or a process

### Execute gitlab runner for each stage in CI

```bash
gitlab-runner exec docker build_script
gitlab-runner exec docker testing
gitlab-runner exec docker code-fixer
```
