<?php

namespace tests\Docker;

use DockerBundle\Services\Docker;
use DockerBundle\Services\DockerFactory;
use PHPUnit\Framework\TestCase;

class DockerFactoryTest extends TestCase
{
    public function test_docker_factory()
    {
        self::assertInstanceOf(Docker::class, DockerFactory::createDocker());
    }
}
